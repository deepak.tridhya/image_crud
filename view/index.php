<?php

include "../model/db.php";

?>

<div class="container">
    <div class="jumbotron text-center">
        <h2>PHP Assignment</h2>
    </div>
    <br>
    <a href="../controller/login.php" role="button" class="btn btn-primary pull-right">Logout</a> 
    <br>
    <br>
    <table class="table table-hover table-striped">
        <tr>
            <th>Sr</th>
            <th>Name</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Hobbies</th>
            <th>Designation</th>
            <th>Mobile</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
<?php  		            

$query = "SELECT * FROM student ORDER BY id ASC ";

$result = mysqli_query($conn,$query);

if(mysqli_num_rows($result) > 0){
    
    while($row = mysqli_fetch_array($result)){
        
        $id    = $row['id'];
        $name  = $row['name'];
        $email = $row['email'];
        $gender = $row['gender'];
        $hobbies = $row['hobbies'];
        $designation = $row['designation'];
        $mobile = $row['mobile'];
        $image = $row['image'];

?>
        
        <tr>
            <td><?=$id; ?></td>
            <td><?=$name; ?></td>
            <td><?=$email; ?></td>
            <td><?=$gender; ?></td>
            <td><?=$hobbies; ?></td>
            <td><?=$designation; ?></td>
            <td><?=$mobile; ?></td>
            <td>
               <img src= "<?= "../images/".$image?>" alt="<?= $name ?>" class="thumbnail" width="100px" height="75px">
            </td>
            <td><a href="../controller/update.php?update=<?php echo $id ?>" class="btn btn-success btn-sm" role="button">Update</a>
            
            <td><a href="../controller/delete.php?delete=<?php echo $id ?>" class="btn btn-danger btn-sm" id="delete" role="button">Delete</a>
            
        </tr>
 <?php
    }
}  
        
 
         
?>

    </table>
</div>


<script>
    $(document).ready(function(){

        $('#delete').click(function(){
            if(!confirm("do you want to delete?"))
            {
                return false;
            }
            else
            {
                return true;
            }
        });


    });
</script>



