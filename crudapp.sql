-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 02, 2023 at 11:14 AM
-- Server version: 8.0.31
-- PHP Version: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crudapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `gender` varchar(191) NOT NULL,
  `hobbies` varchar(191) NOT NULL,
  `designation` varchar(191) NOT NULL,
  `mobile` varchar(191) NOT NULL,
  `image` varchar(191) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `email`, `gender`, `hobbies`, `designation`, `mobile`, `image`, `password`) VALUES
(1, 'Johndoe', 'johndoe@gmail.com', 'Male', 'Hocky', 'Developer', '7318560109', 'bg2.jpg', 'john@123'),
(12, 'Smit Shah', 'smt@gmail.com', 'Male', 'Cricket', 'Developer', '7318560108', 'bg2.jpg', 'smit@123'),
(13, 'Smit Shah', 'smit@gmail.com', 'Male', 'Cricket', 'Developer', '7318560108', 'bg2.jpg', 'deepak@123'),
(15, 'Mohit', 'mohit@gmail.com', 'Male', 'Cricket', 'Developer', '7318560108', 'bg2.jpg', 'Mohit@123'),
(16, 'Pratush Sharma', 'psh@gmail.com', 'Male', 'Cricket', 'Developer', '7318560108', 'bg2.jpg', 'pratush@123');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
