<?php

include "../model/db.php";


if(isset($_POST['register']))
{
   
    $name         = clean($_POST['name']);
    $email        = clean($_POST['email']);
    $gender       = clean($_POST['gender']);
    $hobbies      = clean($_POST['hobbies']);
    $designation  = clean($_POST['designation']);
    $mobile       = clean($_POST['mobile']);
    $image_name   = $_FILES['image']['name'];
    $image        = $_FILES['image']['tmp_name'];
    $password     = clean($_POST['password']);
    $location     = "images/".$image_name;


   
   move_uploaded_file($image, $location);


    $query = "INSERT INTO student (name,email,gender,hobbies,designation,mobile,image,password) VALUES ('".escape($name)."','".escape($email)."' , '".escape($gender)."' , '".escape($hobbies)."', '".escape($designation)."', '".escape($mobile)."','$image_name', '".escape($password)."')";

    $result = mysqli_query($conn,$query) ;

    if($result == true)
    {
        header("Location:view/index.php");
    }
    else
    {
        die('error' . mysqli_error($conn));
    }
}




?>
<div class="container">

    <div class="jumbotron text-center">
        <h2>Register to create account</h2>
    </div>
    <br>
<div class="row">
<div class="col-md-12">
    
<form action="#" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" name="name" class="form-control" placeholder="Enter Name">
    </div>
   
    <div class="form-group">
        <label for="name">Email:</label>
        <input type="text" name="email" class="form-control" placeholder="Enter email">
    </div>
    <div class="form-group">
        <label for="name">Gender:</label>
        <input type="text" name="gender" class="form-control" placeholder="Enter gender">
    </div>

    <div class="form-group">
        <label for="name">Hobbies:</label>
        <input type="text" name="hobbies" class="form-control" placeholder="Enter Hobbies">
    </div>
    <div class="form-group">
        <label for="name">Designation:</label>
        <input type="text" name="designation" class="form-control" placeholder="Enter designation">
    </div>
    <div class="form-group">
        <label for="name">Mobile:</label>
        <input type="text" name="mobile" class="form-control" placeholder="Enter mobile">
    </div>
    <div class="form-group">
        <label for="name">Image:</label>
        <input type="file" class="btn btn-primary" name="image" class="form-control" placeholder="Enter email">
    </div>
    <div class="form-group">
        <label for="name">Password:</label>
        <input type="password" name="password" class="form-control" placeholder="Enter password">
    </div>
   
    <div class="form-group">
        <input type="submit" class="btn btn-success" value="Register" name="register">
    </div>
    <p>Already have an account? <a href="login.php">Login here</a>.</p>
</form>
</div>
</div>

</div>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
