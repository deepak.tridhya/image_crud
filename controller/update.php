<?php
include("../model/db.php");
//include 'model/db.php';
?>

<?php 

if(isset($_GET['update'])){
    $id = $_GET['update'];
    

$query = "SELECT * FROM student WHERE id = $id";

$result = mysqli_query($conn,$query);

if(mysqli_num_rows($result) > 0){
    
    while($row = mysqli_fetch_array($result)){
        
            $name  = $row['name'];
            $email = $row['email'];
            $gender = $row['gender'];
            $hobbies = $row['hobbies'];
            $designation = $row['designation'];
            $mobile = $row['mobile'];
            $image = $row['image'];

        }
    }
}

if(isset($_POST['update'])){
    

    $name         = clean($_POST['name']);
    $email        = clean($_POST['email']);
    $gender       = clean($_POST['gender']);
    $hobbies      = clean($_POST['hobbies']);
    $designation  = clean($_POST['designation']);
    $phone        = clean($_POST['mobile']);
    $image_name   = $_FILES['image']['name'];
    $image        = $_FILES['image']['tmp_name'];

    $location     = "../images/".$image_name;

    move_uploaded_file($image, $location);

    $query  = "UPDATE student SET ";
    $query .= "name = '".escape($name)."', ";
    $query .= "email = '".escape($email)."', ";
    $query .= "gender = '".escape($gender)."', ";
    $query .= "hobbies = '".escape($hobbies)."', ";
    $query .= "designation = '".escape($designation)."', ";
    $query .= "mobile = '".escape($mobile)."', ";
    $query .= "image = '{$image_name}' ";
    $query .= "WHERE id = {$id} ";
    
    $result = mysqli_query($conn,$query);
    
    if($result){
        
        header('location:../view/index.php');
    }
    else
    {
        die('error' . mysql_error($conn));
    }
    
}

?>
<div class="container">
    <div class="jumbotron text-center">
        <h2>Update Your Data</h2>
    </div>
    <br>
<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" name="name" class="form-control" placeholder="Enter Name" value="<?php echo $name ?>">
    </div>
    
    <div class="form-group">
        <label for="name">Email:</label>
        <input type="text" name="email" class="form-control" placeholder="Enter email" value="<?php echo $email ?>">
    </div>
    <div class="form-group">
        <label for="name">Gender:</label>
        <input type="text" name="gender" class="form-control" placeholder="Enter gender" value="<?php echo $gender ?>">
    </div>
    <div class="form-group">
        <label for="name">Hobbies:</label>
        <input type="text" name="hobbies" class="form-control" placeholder="Enter hobbies" value="<?php echo $hobbies ?>">
    </div>
    <div class="form-group">
        <label for="name">Designation:</label>
        <input type="text" name="designation" class="form-control" placeholder="Enter designation" value="<?php echo $designation ?>">
    </div>
    <div class="form-group">
        <label for="name">Mobile:</label>
        <input type="text" name="mobile" class="form-control" placeholder="Enter mobile" value="<?php echo $mobile ?>">
    </div>
    <div class="form-group">
        <label for="name">Image:</label>
        <img src= "<?= "../images/".$image?>" alt="" width="100px" height="100px" class="thumbnail">
        <input type="file" name="image" class="form-control" placeholder="Enter email" value="<?php echo $email ?>">
    </div>

    <div class="form-group">
        <input type="submit" class="btn btn-success" value="Update data" name="update">
    </div>
</form>

</div>
